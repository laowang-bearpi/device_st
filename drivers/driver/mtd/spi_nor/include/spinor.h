#ifndef __SPINOR_H__
#define __SPINOR_H__

// #include "linux/mtd/mtd.h"

struct erase_info {
    int scrub;
    struct erase_info *next;
    unsigned char state;
    unsigned long priv;
    void (*callback) (struct erase_info *self);
    unsigned int cell;
    unsigned int dev;
    unsigned long retries;
    unsigned long time;
    uint64_t fail_addr;
    uint64_t len;
    uint64_t addr;
    struct MtdDev *mtd;
};

// int spinor_init(void);
// void spinor_register(struct mtd_info *mtd);

#endif

