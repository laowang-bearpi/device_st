#ifndef __HOST_COMMON_H__
#define __HOST_COMMON_H__

#include "sys/types.h"
#include "los_mux.h"

#include "mtd_common.h"
#include "spinor_common.h"
#include "spi_common.h"

#define reg_read(_host, _reg) \
        mtd_readl((UINTPTR)((char *)_host->regbase + (_reg)))

#define reg_write(_host, _value, _reg) \
        mtd_writel((unsigned)(_value), (UINTPTR)((char *)_host->regbase + (_reg)))

#define get_host(_host) \
        if(LOS_OK != LOS_MuxLock(&(_host)->lock, LOS_WAIT_FOREVER)) \
            return -1;

#define put_host(_host) \
        if(LOS_OK != LOS_MuxUnlock(&(_host)->lock)) \
            return -1;
struct spinor_host {
    struct spinor_info *spinor;

    char     *regbase;
    char     *membase;

    void (*set_system_clock)(unsigned clock, int clk_en);
    void (*set_host_addr_mode)(struct spinor_host *host, int enable);

    char *buffer;
    char *dma_buffer;
    char *dma_buffer_bak;

    int num_chip;
    struct spi spi[1];

    LosMux lock;
};

#endif /* End of __HOST_COMMON_H__ */

