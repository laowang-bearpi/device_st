#include "los_config.h"
#if defined(LOSCFG_FS_YAFFS) || defined(LOSCFG_FS_JFFS)

#include "fs/fs.h"
#include "stdio.h"
#include "string.h"
#include "errno.h"

#include "los_mux.h"
#include "mtd_dev.h"

#include "mtd_partition.h"
#include "user_copy.h"

/*
 * open device interface
 */
static int mtdchar_open(struct file *filep)
{
    struct Vnode *vnode_p = filep->f_vnode;
    mtd_partition *partition = (mtd_partition *)(vnode_p->data);
	// struct drv_data *data = NULL;

    if (partition->user_num != 0) { // be opened
        return -EBUSY;
    }

    struct MtdDev *mtd = (struct MtdDev *)(partition->mtd_info);
    size_t block_size = mtd->eraseSize;

    (void)LOS_MuxLock(&partition->lock, LOS_WAIT_FOREVER);

    partition->user_num = 1;
    filep->f_pos = partition->start_block * block_size;

    (void)LOS_MuxUnlock(&partition->lock);

    return ENOERR;
}

/*
 * close device interface
 */
static int mtdchar_close(struct file *filep)
{
    struct Vnode *vnode_p = filep->f_vnode;
    mtd_partition *partition = (mtd_partition *)(vnode_p->data);

    (void)LOS_MuxLock(&partition->lock, LOS_WAIT_FOREVER);

    partition->user_num = 0;

    (void)LOS_MuxUnlock(&partition->lock);

    return ENOERR;
}

/*
 * read device interface
 */
static ssize_t mtdchar_read(struct file *filep, char *buffer, size_t buflen)
{
    struct Vnode *vnode_p = filep->f_vnode;
    mtd_partition *partition = (mtd_partition *)(vnode_p->data);

    (void)LOS_MuxLock(&partition->lock, LOS_WAIT_FOREVER);

    struct MtdDev *mtd = (struct MtdDev *)(partition->mtd_info);
    uint64_t block_size = mtd->eraseSize;
    uint64_t start_addr = partition->start_block * block_size;
    uint64_t end_addr = (partition->end_block + 1) * block_size;

    uint64_t retlen;
    ssize_t ret = 0;


    if (!buflen) {
        ret = 0;
        goto out1;
    }

    retlen = mtd->read(mtd, start_addr, end_addr - start_addr, buffer);

    if (retlen < 0) {
        goto out1;
    }

    filep->f_pos += retlen;

    ret = (ssize_t)retlen;

out1:
    (void)LOS_MuxUnlock(&partition->lock);
    return ret;
}

/*
 * write device interface
 */
static ssize_t mtdchar_write(struct file *filep, const char *buffer, size_t buflen)
{
    struct Vnode *vnode_p = filep->f_vnode;
    mtd_partition *partition = (mtd_partition *)(vnode_p->data);

    (void)LOS_MuxLock(&partition->lock, LOS_WAIT_FOREVER);

    struct MtdDev *mtd = (struct MtdDev *)(partition->mtd_info);
    uint64_t block_size = mtd->eraseSize;
    uint64_t start_addr = partition->start_block * block_size;
    uint64_t end_addr = (partition->end_block + 1) * block_size;
    uint64_t retlen;
    int ret = 0;

    if (!buflen) {
        ret = 0;
        goto out1;
    }

    retlen = mtd->write(mtd, start_addr, end_addr - start_addr, buffer);

    if (retlen < 0) {
        goto out1;
    }

    filep->f_pos += retlen;

    ret = (ssize_t)retlen;

out1:
    (void)LOS_MuxUnlock(&partition->lock);
    return ret;
}

/*
 * lseek device interface
 */
static off_t mtdchar_lseek(struct file *filep, off_t offset, int whence)
{
    struct Vnode *vnode_p = filep->f_vnode;
    mtd_partition *partition = (mtd_partition *)(vnode_p->data);

    (void)LOS_MuxLock(&partition->lock, LOS_WAIT_FOREVER);

    struct MtdDev *mtd = (struct MtdDev *)(partition->mtd_info);
    size_t block_size = mtd->eraseSize;
    size_t end_addr = (partition->end_block + 1) * block_size;
    size_t start_addr = partition->start_block * block_size;

    switch (whence) {
        case SEEK_SET:
            if (offset >= 0 && (size_t)offset < end_addr - start_addr) {
                filep->f_pos = start_addr + offset;
                goto out1;
            } else {
                goto err1;
            }

        case SEEK_CUR:
            if (offset + (size_t)filep->f_pos >= start_addr &&
                    (size_t)(offset + filep->f_pos) < end_addr) {
                filep->f_pos += offset;
                goto out1;
            } else {
                goto err1;
            }

        case SEEK_END:
            if (offset < 0 && offset + end_addr >= start_addr) {
                filep->f_pos = (off_t)(offset + end_addr);
                goto out1;
            } else {
                goto err1;
            }

        default:
            goto err1;
    }
err1:
    (void)LOS_MuxUnlock(&partition->lock);
    return -EINVAL;
out1:
    (void)LOS_MuxUnlock(&partition->lock);
    return filep->f_pos;
}

static ssize_t mtdchar_map(struct file* filep, LosVmMapRegion *region)
{
    PRINTK("%s %d, mmap is not support\n", __FUNCTION__, __LINE__);
    return 0;
}

const struct file_operations_vfs g_mtdchar_fops = {
    .open   =   mtdchar_open,
    .close  =   mtdchar_close,
    .read   =   mtdchar_read,
    .write  =   mtdchar_write,
    .seek   =   mtdchar_lseek,
    .mmap   =   mtdchar_map,
#ifndef CONFIG_DISABLE_POLL
    .poll   =   NULL,
#endif
    .unlink =   NULL,
};

#endif