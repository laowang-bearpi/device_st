#include "stdio.h"
#include "los_typedef.h"
// #include "asm/platform.h"
#include "los_base.h"
#include "los_vm_phys.h"
//#include "asm/dma.h"

extern int snprintf_s(char *str, size_t sizeOfBuffer, size_t n, const char *fmt, ...);
/*----------------------------------------------------------------------------*/
/* ulltostr */
/*----------------------------------------------------------------------------*/
char *ulltostr(unsigned long long size)
{
    int ix;
    static char buffer[20];
    unsigned long size_long;
    char *fmt[] = {"%u", "%uK", "%uM", "%uG", "%uT", "%uP"};

    for (ix = 0; (ix < 5) && !(size & 0x3FF) && size; ix++) {
        size = (size >> 10);
    }

    size_long = (unsigned long)size;

    snprintf_s(buffer, sizeof(buffer), sizeof(buffer) - 1, fmt[ix], size_long);

    return buffer;
}

/*----------------------------------------------------------------------------*/
/* ffs */
/*----------------------------------------------------------------------------*/
int ffs(int x)
{
    int r = 1;
    unsigned int f = (unsigned int)x;

    if (!f) {
        return 0;
    }

    if (!(f & 0xffff)) {
        f >>= 16;
        r += 16;
    }
    if (!(f & 0xff)) {
        f >>= 8;
        r += 8;
    }
    if (!(f & 0xf)) {
        f >>= 4;
        r += 4;
    }
    if (!(f & 3)) {
        f >>= 2;
        r += 2;
    }
    if (!(f & 1)) {
        r += 1;
    }
    return r;
}
#if 0
/*----------------------------------------------------------------------------*/
/* mtd_dma_cache_inv */
/*----------------------------------------------------------------------------*/
void mtd_dma_cache_inv(void *addr, unsigned int size)
{
    addr = (void *)(UINTPTR)LOS_PaddrToKVaddr((PADDR_T)(UINTPTR)addr);

    UINTPTR start = (UINTPTR)addr & ~(CACHE_ALIGNED_SIZE - 1);
    UINTPTR end = (UINTPTR)addr + size;

    end = ALIGN(end, CACHE_ALIGNED_SIZE);

    dma_cache_inv(start, end);
}

/*----------------------------------------------------------------------------*/
/* mtd_dma_cache_clean */
/*----------------------------------------------------------------------------*/
void mtd_dma_cache_clean(void *addr, unsigned int size)
{
    addr = (void *)(UINTPTR)LOS_PaddrToKVaddr((PADDR_T)(UINTPTR)addr);
    UINTPTR start = (UINTPTR)addr & ~(CACHE_ALIGNED_SIZE - 1);
    UINTPTR end = (UINTPTR)addr + size;

    end = ALIGN(end, CACHE_ALIGNED_SIZE);

    dma_cache_clean(start, end);
}
#endif
