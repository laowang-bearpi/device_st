#include "fs/fs.h"
// #include "inode/inode.h"

#include "mtd_common.h"
static int mtdblock_open(struct Vnode *filep)
{
    return 0;
}

static int mtdblock_close(struct Vnode *filep)
{
    return 0;
}

static ssize_t mtdblock_read(struct Vnode *filep, unsigned char *buffer,
                             unsigned long long start_sector, unsigned int nsectors)
{
    return 0;
}

static ssize_t mtdblock_write(struct Vnode *filep, const unsigned char *buffer,
                              unsigned long long start_sector, unsigned int nsectors)
{
    return 0;
}

static int mtdblock_geometry(struct Vnode *filep, struct geometry *geometry_p)
{
    return 0;
}

static int mtdblock_ioctl(struct Vnode *filep, int cmd, unsigned long arg)
{
    return 0;
}

const struct block_operations g_dev_nand_ops = {
    .open       = mtdblock_open,
    .close      = mtdblock_close,
    .read       = mtdblock_read,
    .write      = mtdblock_write,
    .geometry   = mtdblock_geometry,
    .ioctl      = mtdblock_ioctl,
    .unlink     = NULL
};

const struct block_operations g_dev_spinor_ops = {
    .open       = mtdblock_open,
    .close      = mtdblock_close,
    .read       = mtdblock_read,
    .write      = mtdblock_write,
    .geometry   = mtdblock_geometry,
    .ioctl      = mtdblock_ioctl,
    .unlink     = NULL
};

