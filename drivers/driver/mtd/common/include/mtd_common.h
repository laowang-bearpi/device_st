#ifndef __MTD_COMMON_H__
#define __MTD_COMMON_H__

#include "stdint.h"

/*---------------------------------------------------------------------------*/
/* base type macros */
/*---------------------------------------------------------------------------*/
#ifndef u16
#define u16         unsigned short
#endif
#ifndef u32
#define u32         unsigned int
#endif
#ifndef ulong
#define ulong       unsigned long
#endif

/*---------------------------------------------------------------------------*/
/* frequently-used macros */
/*---------------------------------------------------------------------------*/
#ifndef min
#define min(x,y) (x<y?x:y)
#endif
#ifndef max
#define max(x,y) (x<y?y:x)
#endif
#ifndef min_t
#define min_t(t, x,y) ((t)x<(t)y?(t)x:(t)y)
#endif

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))
#endif

/*---------------------------------------------------------------------------*/
/* mtd device capacity mcaros */
/*---------------------------------------------------------------------------*/
#define _256B       (256)
#define _512B       (512)
#define _1K         (1024)
#define _2K         (2048)
#define _4K         (4096)
#define _8K         (8192)
#define _16K        (16384)
#define _32K        (32768)
#define _64K        (0x10000UL)
#define _128K       (0x20000UL)
#define _256K       (0x40000UL)
#define _512K       (0x80000UL)
#define _1M         (0x100000UL)
#define _2M         (0x200000UL)
#define _4M         (0x400000UL)
#define _8M         (0x800000UL)
#define _16M        (0x1000000UL)
#define _32M        (0x2000000UL)
#define _64M        (0x4000000UL)
#define _128M       (0x8000000UL)
#define _256M       (0x10000000UL)
#define _512M       (0x20000000UL)
#define _1G         (0x40000000ULL)
#define _2G         (0x80000000ULL)
#define _4G         (0x100000000ULL)
#define _8G         (0x200000000ULL)
#define _16G        (0x400000000ULL)
#define _64G        (0x1000000000ULL)
#define INFINITE    (0xFFFFFFFF)
/*---------------------------------------------------------------------------*/
/* mtd device print control mcaros */
/*---------------------------------------------------------------------------*/
#include "los_printf.h"
#include "asm/io.h"

#define DISABLE     0
#define ENABLE      1

#define READ        0
#define WRITE       1

#define MTD_REG_DEBUG DISABLE
//#define MTD_REG_DEBUG ENABLE

#define mtd_trace(debug, msg...) do { \
    if (debug == ENABLE) { \
        dprintf("%s:%d: ", __func__, __LINE__); \
        dprintf(msg); \
        dprintf("\n"); \
    } \
} while (0)

#define mtd_readl(addr) ({unsigned int reg = readl((UINTPTR)addr); \
        mtd_trace(MTD_REG_DEBUG, "readl(0x%p) = 0x%08X", (UINTPTR)addr, reg); \
        reg; })

#define mtd_writel(v, addr) do { \
    writel(v, (UINTPTR)addr); \
    mtd_trace(MTD_REG_DEBUG, "writel(0x%p) = 0x%08X",\
            (UINTPTR)addr, (unsigned int)(v)); \
} while (0)

/*****************************************************************************/
#define INIT_DBG      0        /* Init  debug print */
#define ER_DBG        0        /* Erase debug print */
#define WR_DBG        0        /* Write debug print */
#define RD_DBG        0        /* Read  debug print */

#define DB_BUG(fmt, args...) \
    do { \
        dprintf("%s(%d): BUG: " fmt, __FILE__, __LINE__, ##args); \
        __asm("b ."); \
    } while (0)

#define DBG_MSG(_fmt, arg...) \
    dprintf("%s(%d): " _fmt, __func__, __LINE__, ##arg);

#define ERR_MSG(_fmt, arg...) \
    dprintf("%s(%d): Error:" _fmt, __func__, __LINE__, ##arg);

#define WARN_MSG(_fmt, arg...) \
    dprintf("%s(%d): Warning:" _fmt, __func__, __LINE__, ##arg);

#define INFO_MSG(_fmt, arg...) \
    dprintf(_fmt, ##arg);

#define MTD_PR(_type, _fmt, arg...) \
    do { \
        if (_type) \
            DBG_MSG(_fmt, ##arg) \
    } while (0)

/* function and variable declaration */

char *ulltostr(unsigned long long size);
int ffs(int x);
void mtd_dma_cache_inv(void *addr, unsigned int size);
void mtd_dma_cache_clean(void *addr, unsigned int size);

extern const struct file_operations_vfs g_mtdchar_fops;
extern const struct block_operations g_dev_spinor_ops;
extern const struct block_operations g_dev_nand_ops;

// static inline const struct file_operations_vfs * GetMtdCharFops(void)
// {
//     return &g_mtdchar_fops;
// }
// static inline const struct block_operations * GetDevSpinorOps(void)
// {
// 	return &g_dev_spinor_ops;
// }

#endif /* End of __MTD_COMMON_H__ */

