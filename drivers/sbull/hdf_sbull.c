#include "sbull.h"

#include "hdf_log.h"
#include "hdf_base.h"
#include "hdf_device_desc.h"
#include "device_resource_if.h"

#include "sbull_internal.h"

int hdfSbullDriverInit(struct HdfDeviceObject *deviceObject)
{
    int ret;
    struct DeviceResourceIface *p = DeviceResourceGetIfaceInstance(HDF_CONFIG_SOURCE);
    struct Sbull *sbull = NULL;
    uint32_t vbase;
    uint32_t size;
    if (deviceObject == NULL ||  deviceObject->property == NULL) {
        dprintf("[%s]deviceObject or property is null", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    // 获取flash在内存中的地址
    if ((ret = p->GetUint32(deviceObject->property, "vbase", &vbase, 0))) {
        
        dprintf("[%s]GetUint32 error:%d", __func__, ret);
        return HDF_FAILURE;
    }

    if ((ret = p->GetUint32(deviceObject->property, "size", &size, 0))) {
        dprintf("[%s]GetUint32 error:%d", __func__, ret);
        return HDF_FAILURE;
    }

    // 创建sbull
    sbull = (struct Sbull *)LOS_MemAlloc(m_aucSysMem0, sizeof(struct Sbull));
    if (sbull == NULL) {
        return HDF_FAILURE;
    }

    // init sbull
    ret = sbullInit(sbull, vbase, size);
    if (ret != 0) {
        LOS_MemFree(m_aucSysMem0, sbull);
        sbull = NULL;
        ret = HDF_FAILURE;
        dprintf("sbullInit fail.\r\n");
    }

    dprintf("sbullInit ok.\r\n");

    return ret;
}

struct HdfDriverEntry g_sbullDriverEntry = {
    .moduleVersion = 1,
    .moduleName = "sbull",
    .Bind = NULL,
    .Init = hdfSbullDriverInit,
    .Release = NULL,
};
HDF_INIT(g_sbullDriverEntry);
