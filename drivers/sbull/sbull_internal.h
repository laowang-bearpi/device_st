#ifndef _SBULL_INTERNAL_H_
#define _SBULL_INTERNAL_H_

#include "sbull.h"

#include "fs/driver.h"
#include "mtd_dev.h"
#include "mtd_partition.h"

// 擦除块大小=扇区大小 4K
#define SBULL_ERASE_SIZE    SBULL_SECTOR_SIZE

// mtdDev
void *GetMtd(const char *type);
int FreeMtd(struct MtdDev *mtd);

// Ops
struct block_operations *GetDevSpinorOps(void);
struct file_operations_vfs *GetMtdCharFops(void);

// MTD device
int sbullMtdErase(struct MtdDev *mtd, UINT64 start, UINT64 bytes, UINT64 *failAddr);
int sbullMtdRead(struct MtdDev *mtd, UINT64 start, UINT64 bytes, const char *buf);
int sbullMtdWrite(struct MtdDev *mtd, UINT64 start, UINT64 bytes, const char *buf);

// BLK device
int sbullBlkOpen(struct Vnode *vnode);
int sbullBlkClose(struct Vnode *vnode);
ssize_t sbullBlkRead(struct Vnode *vnode, unsigned char *buffer, unsigned long long start_sector, unsigned int nsectors);
ssize_t sbullBlkWrite(struct Vnode *vnode, const unsigned char *buffer, unsigned long long start_sector, unsigned int nsectors);
int sbullBlkGeometry(struct Vnode *vnode, struct geometry *geometry);

// CHR device
int sbullCharOpen(struct file *filep);
int sbullCharClose(struct file *filep);
ssize_t sbullCharRead(struct file *filep, char *buffer, size_t buflen);
ssize_t sbullCharWrite(struct file *filep, const char *buffer, size_t buflen);
off_t sbullCharSeek(struct file *filep, off_t offset, int whence);

#endif
