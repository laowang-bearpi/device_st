相关结构体说明

struct geometry
{
  bool               geo_available;    /* true: The device is available */
  bool               geo_mediachanged; /* true: The media has changed since last query */
  bool               geo_writeenabled; /* true: It is okay to write to this device */
  unsigned long long geo_nsectors;     /* Number of sectors on the device */
  size_t             geo_sectorsize;   /* Size of one sector */
};

struct block_operations
{
  int     (*open)(struct Vnode *vnode);
  int     (*close)(struct Vnode *vnode);
  ssize_t (*read)(struct Vnode *vnode, unsigned char *buffer,
            unsigned long long start_sector, unsigned int nsectors);
  ssize_t (*write)(struct Vnode *vnode, const unsigned char *buffer,
            unsigned long long start_sector, unsigned int nsectors);
  int     (*geometry)(struct Vnode *vnode, struct geometry *geometry);
  int     (*ioctl)(struct Vnode *vnode, int cmd, unsigned long arg);
  int     (*unlink)(struct Vnode *vnode);
};

struct file_operations_vfs
{
  /* The device driver open method differs from the mountpoint open method */

  int     (*open)(struct file *filep);

  /* The following methods must be identical in signature and position because
   * the struct file_operations and struct mountp_operations are treated like
   * unions.
   */

  int     (*close)(struct file *filep);
  ssize_t (*read)(struct file *filep, char *buffer, size_t buflen);
  ssize_t (*write)(struct file *filep, const char *buffer, size_t buflen);
  off_t   (*seek)(struct file *filep, off_t offset, int whence);
  int     (*ioctl)(struct file *filep, int cmd, unsigned long arg);
  int     (*mmap)(struct file* filep, struct VmMapRegion *region);
  /* The two structures need not be common after this point */

  int     (*poll)(struct file *filep, poll_table *fds);
  int     (*stat)(struct file *filep, struct stat* st);
  int     (*fallocate)(struct file* filep, int mode, off_t offset, off_t len);
  int     (*fallocate64)(struct file *filep, int mode, off64_t offset, off64_t len);
  int     (*fsync)(struct file *filep);
  ssize_t (*readpage)(struct file *filep, char *buffer, size_t buflen);
  int     (*unlink)(struct Vnode *vnode);
};

typedef struct mtd_node {
    UINT32 start_block;
    UINT32 end_block;
    UINT32 patitionnum;
    CHAR *blockdriver_name;
    CHAR *chardriver_name;
    CHAR *mountpoint_name;

    // struct MtdDev *flash_mtd;
    VOID *mtd_info; /* Driver used by a partition */

    LOS_DL_LIST node_info;
    LosMux lock;
    UINT32 user_num;
} mtd_partition;

typedef struct par_param {
    mtd_partition *partition_head;
    struct MtdDev *flash_mtd;
    const struct block_operations *flash_ops;
    const struct file_operations_vfs *char_ops;
    CHAR *blockname;
    CHAR *charname;
    UINT32 block_size;
} partition_param;

struct MtdDev {
    VOID *priv;
    UINT32 type;

    UINT64 size;
    UINT32 eraseSize;

    int (*erase)(struct MtdDev *mtd, UINT64 start, UINT64 len, UINT64 *failAddr);
    int (*read)(struct MtdDev *mtd, UINT64 start, UINT64 len, const char *buf);
    int (*write)(struct MtdDev *mtd, UINT64 start, UINT64 len, const char *buf);
};
