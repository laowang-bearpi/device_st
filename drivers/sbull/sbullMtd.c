/*
sbull MTD设备驱动
*/
#include "sbull.h"
#include "sbull_internal.h"

int sbullMtdErase(struct MtdDev *mtd, UINT64 start, UINT64 bytes, UINT64 *failAddr)
{
    struct Sbull *sbull = NULL;

    if (mtd == NULL || mtd->priv == NULL) {
        return -1;
    }
    sbull = (struct Sbull *)mtd->priv;

    // 判断是否溢出
    if (start + bytes > mtd->size) {
        return -EINVAL;
    }

    // 判断是否对齐
    if (start & (mtd->eraseSize - 1)) {
        return -EINVAL;
    }

    // 判断是否对齐
    if (bytes & (mtd->eraseSize - 1)) {
        return -EINVAL;
    }

    memset((sbull->base + start), 0xff, bytes);
    return 0;
}

int sbullMtdRead(struct MtdDev *mtd, UINT64 start, UINT64 bytes, const char *buf)
{
    struct Sbull *sbull = NULL;

    if (mtd == NULL || mtd->priv == NULL) {
        return -1;
    }
    sbull = (struct Sbull *)mtd->priv;

    // 判断是否溢出
    if ((start + bytes) > mtd->size) {
        return -EINVAL;
    }

    // 判断长度是否非0
    if (!bytes) {
        return 0;
    }

    memcpy((void *)buf, (void *)(sbull->base + start), bytes);
    return bytes;
}

int sbullMtdWrite(struct MtdDev *mtd, UINT64 start, UINT64 bytes, const char *buf)
{
    struct Sbull *sbull = NULL;

    if (mtd == NULL || mtd->priv == NULL) {
        return -1;
    }
    sbull = (struct Sbull *)mtd->priv;

    // 判断是否溢出
    if ((start + bytes) > mtd->size) {
		return -EINVAL;
    }

    // 判断长度是否非0
    if (!bytes) {
		return 0;
    }

    memcpy((void *)(sbull->base + start), (void *)buf, bytes);
    return bytes;
}
