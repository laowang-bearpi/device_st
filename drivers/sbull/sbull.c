/*
sbull 驱动
*/
#include "sbull.h"
#include "sbull_internal.h"

// MTD
static struct MtdDev g_sbullMtdDev = {
    .priv = NULL,
    .type = MTD_NORFLASH,
    .size = 0,
    .eraseSize = SBULL_ERASE_SIZE,
    .erase = sbullMtdErase,
    .read = sbullMtdRead,
    .write = sbullMtdWrite,
};

// BLK
static struct block_operations g_sbullBlkOps = { 
    .open       = sbullBlkOpen,
    .close      = sbullBlkClose,
    .read       = sbullBlkRead,
    .write      = sbullBlkWrite,
    .geometry   = sbullBlkGeometry
};

// Chr
static struct file_operations_vfs g_sbullChrOps = {
    .open   = sbullCharOpen,
    .close  = sbullCharClose,
    .read   = sbullCharRead,
    .write  = sbullCharWrite,
    .seek   = sbullCharSeek
};

void *GetMtd(const char *type)
{
    (void)type;// if type == "spinor"
    return &g_sbullMtdDev;
}

int FreeMtd(struct MtdDev *mtd)
{
    /* do nothing. */
    (void)mtd;
    return 0;
}

struct block_operations *GetDevSpinorOps(void)
{
    return &g_sbullBlkOps;
}

struct block_operations *GetDevNandOps(void)
{
    return &g_sbullBlkOps;
}

struct file_operations_vfs *GetMtdCharFops(void)
{
    return &g_sbullChrOps;
}

int32_t sbullInit(struct Sbull *sbull, uint32_t vbase, uint32_t size)
{
    struct MtdDev *mtd = GetMtd("spinor");

    if (sbull == NULL || mtd == NULL) return -1;

    // 填充sbull属性
    sbull->base = (uint8_t *)vbase;
    sbull->size = size;

    // 填充mtd属性
    mtd->priv = (void *)sbull;
    mtd->size = size;

    return 0;
}

