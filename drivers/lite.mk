# Copyright (c) 2020-2021 YangZeLin. All rights reserved.
STM32MP157_BASE_DIR  := $(LITEOSTOPDIR)/../../device/st/drivers

# # mtd_common
# LIB_SUBDIRS     += $(STM32MP157_BASE_DIR)/driver/mtd/common
# LITEOS_BASELIB  += -lmtd_common

# # spi_nor
# LIB_SUBDIRS     += $(STM32MP157_BASE_DIR)/driver/mtd/spi_nor
# LITEOS_BASELIB  += -lspinor_flash

# sbull
LIB_SUBDIRS     += $(STM32MP157_BASE_DIR)/sbull
LITEOS_BASELIB += -lhdf_sbull

ifeq ($(LOSCFG_DRIVERS_HDF_PLATFORM_UART), y)
LIB_SUBDIRS     += $(STM32MP157_BASE_DIR)/uart
LITEOS_BASELIB += -lhdf_uart
endif
		
ifeq ($(LOSCFG_DRIVERS_HDF_PLATFORM_WATCHDOG), y)
LIB_SUBDIRS     += $(STM32MP157_BASE_DIR)/watchdog
LITEOS_BASELIB += -lhdf_watchdog
endif

# smp
ifeq ($(LOSCFG_KERNEL_SMP), y)
LIB_SUBDIRS     += $(STM32MP157_BASE_DIR)/mp1_smp
LITEOS_BASELIB += -lmp1_smp
endif


